import * as express from 'express';
import {AppRouter} from './router';

const app = express();

app.use(AppRouter);

app.listen(3000, () => {
    console.log('listening on port 3000')
});

