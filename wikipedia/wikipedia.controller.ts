import { Request, response, Response } from 'express';
import request = require('request');
import { parse } from 'node-html-parser';
import rp = require('request-promise');

export class WikipediaController {

    static async getArticle(req: Request, res: Response) {
        {
            // regex validation
            const articleName = req.query.articleName;

            const options = {
                url: `https://he.wikipedia.org/wiki/${articleName}`,
                headers: {
                    'Accept-language': 'fr'
                },
                transform: (body: any) => {
                    const root = parse(body);
                    const introduction = parse([root.querySelector('.vector-body p')?.childNodes].toString());
            
                    const responseBody = {
                        scrapeDate: new Date(),
                        articleName,
                        introduction: introduction.rawText
                    }

                    return(responseBody);
                    
                }
            };

            const articleObject = await rp(options);

            res.send(articleObject);
        }
    }

}