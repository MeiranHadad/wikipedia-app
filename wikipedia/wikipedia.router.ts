import { Router } from 'express';
import { WikipediaController } from './wikipedia.controller';

const wikipediaRouter: Router = Router();

wikipediaRouter.get('/', WikipediaController.getArticle);


export { wikipediaRouter }
