import { Router } from 'express';
import { wikipediaRouter } from './wikipedia/wikipedia.router';

const AppRouter: Router = Router();

AppRouter.use('/api/introduction', wikipediaRouter);

AppRouter.use('*', (req, res) => {
    res.status(404).send('Invalid Route');
});
  
  export { AppRouter };

